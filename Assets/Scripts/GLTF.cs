﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniGLTF;
using UnityEditor;
using UnityEngine;


public class GLTF : MonoBehaviour
{
    // string path; // gltf, glb or zip(include gltf)
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadFile(string name, string path, float boundSize = 1000f)
    {
        var context = new ImporterContext();
        // use string name as object name in the scene

        try
        {
            context.Load(path);            
        }
        catch (Exception e)
        {
            Debug.LogError("Unable to Loading Asset from File: " + path);
            return;
        }

        context.EnableUpdateWhenOffscreen();
        context.ShowMeshes();

        GameObject root = context.Root;
        root.tag = "Imported"; // set tag as imported

        foreach (SkinnedMeshRenderer smr in root.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            smr.localBounds = new Bounds(new Vector3(0,0,0), new Vector3(boundSize, boundSize, boundSize));
            // resize bounds
        }

        foreach (Transform child in root.GetComponentsInChildren<Transform>())
        {
            child.gameObject.tag = "Imported";
            // mark all child objects as imported.
        }

        if (!string.IsNullOrEmpty(name))
        {
            root.name = name;
        }
    }

    public void PlayAnimation(string objectName, string animationName)
    {
        GameObject currentObject = GameObject.Find(objectName);
        if (string.IsNullOrEmpty(animationName))
        {
            currentObject.GetComponent<Animation>().Play();
        }
        else
        {
            currentObject.GetComponent<Animation>().Play(animationName);
        }
        
    }

    public void StopAnimation(string objectName)
    {
        GameObject currentObject = GameObject.Find(objectName);
        currentObject.GetComponent<Animation>().Stop();
    }

    public void Translate(string objectName, Vector3 position)
    {
        GameObject currentObject = GameObject.Find(objectName);
        currentObject.transform.Translate(position);
    }

    public void Rotate(string objectName, Vector3 rotation)
    {
        GameObject currentObject = GameObject.Find(objectName);
        currentObject.transform.Rotate(rotation);
    }
}
