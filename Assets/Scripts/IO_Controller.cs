﻿using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
// using UnityEditor;
using UnityEngine;

public class IO_Controller : MonoBehaviour
{

    private IO_Loader loader;
    private IO_CommandProcessor processor;
    private IO_ErrorChecking checker;
    // Start is called before the first frame update
    void Start()
    {
        checker = GetComponent<IO_ErrorChecking>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadFile() // this method should be used by default
    {
        string path = null;
        // use string name as object name in the scene
        while (string.IsNullOrEmpty(path))
        {
            // path = EditorUtility.OpenFilePanel("Select Datafile", "", "txt");

            try
            {
                path = StandaloneFileBrowser.OpenFilePanel("Select INS Datafile", "", "txt", false)[0];
            }
            catch (Exception e)
            {
                Debug.LogError("Unable to Load file.");
                return;
            }
        }

        LoadFile(path);
        Debug.LogWarning(path);
    }
    public void LoadFile(string filePath)
    {
        loader = GetComponent<IO_Loader>();
        processor = GetComponent<IO_CommandProcessor>();

        loader.setFilePath(filePath);
        processor.SetPath(filePath);

        while (true)
        {
            List<string> arguments = loader.nextLine();

            if (arguments == null)
            {
                break; // stop loading after loader returns null
            }

            processor.process(arguments);
        }
    }

    public void TogglePause()
    {
        processor.TogglePause();
    }

    public bool IsPlaying()
    {
        return processor.IsPlaying();
    }

    public void ClearScene()
    {
        processor.ClearScene();
    }
}
