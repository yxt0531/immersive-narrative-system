﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    const float WALKING_SPEED = 2;
    const float RUNNING_SPEED = 5;

    GLTF gltf;
    List<MovableObject> movingObjects;
    List<RotatableObject> rotatingObjects;
    bool isPlaying;

    // Start is called before the first frame update
    void Start()
    {
        try
        {
            gltf = GameObject.Find("GLTF").GetComponent<GLTF>();
        }
        catch (NullReferenceException e)
        {
            Debug.LogError("Unable to load GLTF module, imported objects animation disabled, please place GLTF prefab under root of current scene.");
        }
        movingObjects = new List<MovableObject>();
        rotatingObjects = new List<RotatableObject>();
        isPlaying = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlaying && (movingObjects.Count > 0 || rotatingObjects.Count > 0))
        {
            for (int i = movingObjects.Count - 1; i >= 0; i--)
            {
                if (movingObjects[i].currentLerpTime > movingObjects[i].duration)
                {
                    if (movingObjects[i].gameObject.GetComponent<Animator>() != null)
                    {
                        // change back to idle animation if object has animator
                        movingObjects[i].gameObject.GetComponent<Animator>().Play("Idle");
                    }
                    movingObjects.RemoveAt(i); // remove game object from list if animation is finished
                }
                else
                {
                    movingObjects[i].currentLerpTime += Time.deltaTime;
                    movingObjects[i].progress = movingObjects[i].currentLerpTime / movingObjects[i].duration;
                    movingObjects[i].gameObject.transform.position = Vector3.Lerp(movingObjects[i].origin, movingObjects[i].destination, movingObjects[i].progress);
                }
            }

            for (int i = rotatingObjects.Count - 1; i >= 0; i--)
            {
                if (rotatingObjects[i].currentLerpTime > rotatingObjects[i].duration)
                {
                    rotatingObjects.RemoveAt(i); // remove game object from list if animation is finished
                }
                else
                {
                    rotatingObjects[i].currentLerpTime += Time.deltaTime;
                    rotatingObjects[i].progress = rotatingObjects[i].currentLerpTime / rotatingObjects[i].duration;
                    rotatingObjects[i].gameObject.transform.rotation = Quaternion.Lerp(rotatingObjects[i].fromRotation, rotatingObjects[i].toRotation, rotatingObjects[i].progress);
                }
            }
        }
    }

    public void MoveObject(string objectName, Vector3 destination, float duration = 0.0f)
    {
        GameObject currentObject = GameObject.Find(objectName);
        if (currentObject == null)
        {
            Debug.LogError("Cannot find object, check if the name of object is valid.");
        }
        else
        {
            switch(currentObject.tag)
            {
                case "Imported":
                case "Kinematic":
                    Debug.Log("Object is kinematic.");
                    if (duration == 0.0f)
                    {
                        // duration is 0 second, teleport object to destination
                        currentObject.transform.position = destination;
                    }
                    else
                    {
                        MovableObject newMovableObject = new MovableObject();

                        newMovableObject.gameObject = currentObject;
                        newMovableObject.origin = currentObject.transform.position;
                        newMovableObject.destination = destination;
                        newMovableObject.duration = duration;
                        newMovableObject.currentLerpTime = 0.0f;
                        newMovableObject.progress = 0.0f;

                        movingObjects.Add(newMovableObject);
                    }
                    break;
                case "Static":
                    Debug.Log("Object is static and cannot be moved.");
                    break;
                default:
                    break;
            }
        }
    }

    public void RotateObject(string objectName, Quaternion toRotation, float duration = 0.0f)
    {
        GameObject currentObject = GameObject.Find(objectName);
        if (currentObject == null)
        {
            Debug.LogError("Cannot find object, check if the name of object is valid.");
        }
        else
        {
            switch (currentObject.tag)
            {
                case "Imported":
                case "Kinematic":
                    Debug.Log("Object is kinematic.");
                    if (duration == 0.0f)
                    {
                        // duration is 0 second, teleport object to destination
                        currentObject.transform.rotation = toRotation;
                    }
                    else
                    {
                        RotatableObject newRotatableObject = new RotatableObject();

                        newRotatableObject.gameObject = currentObject;
                        newRotatableObject.fromRotation = currentObject.transform.rotation;
                        newRotatableObject.toRotation = toRotation;
                        newRotatableObject.duration = duration;
                        newRotatableObject.currentLerpTime = 0.0f;
                        newRotatableObject.progress = 0.0f;

                        rotatingObjects.Add(newRotatableObject);
                    }
                    break;
                case "Static":
                    Debug.Log("Object is static and cannot be moved.");
                    break;
                default:
                    break;
            }
        }
    }

    public void RotateObject(string objectName, Vector3 toDirection, float duration = 0.0f)
    {
        // rotate by vector direction
        RotateObject(objectName, Quaternion.LookRotation(toDirection, Vector3.up), duration);
    }

    public void PlayAnimation(string objectName, string animationName, Vector3? destination = null, float duration = 0.0f)
    {
        GameObject currentObject = GameObject.Find(objectName);
        Vector3 relativeDirection;

        if (currentObject == null)
        {
            Debug.LogError("Cannot find object, check if the name of object is valid.");
        }
        else
        {
            switch (currentObject.tag)
            {
                case "Imported":
                    // object is imported, calling gltf module
                    gltf.PlayAnimation(objectName, animationName);
                    break;
                default:
                    // play pre-defined animation for built-in objects
                    if (currentObject.GetComponent<Animator>() == null)
                    {
                        Debug.LogError("Object does not have animation attached to it.");
                    }
                    else
                    {
                        switch (animationName)
                        {
                            // TODO more predefined animation command here
                            case "Walking":
                                if (destination == null || duration < 0.0f)
                                {
                                    Debug.LogError("Cannot walk, destination is null or duration is smaller than 0.");
                                }
                                else
                                {
                                    if (duration == 0.0f)
                                    {
                                        duration = Vector3.Distance((Vector3)destination, currentObject.transform.position) / WALKING_SPEED;
                                    }
                                    relativeDirection = (Vector3)destination - currentObject.transform.position;
                                    MoveObject(objectName, (Vector3)destination, duration);
                                    RotateObject(objectName, relativeDirection, 0.5f); // rotate in 0.5 seconds
                                    currentObject.GetComponent<Animator>().Play("Walking");
                                }
                                break;
                            case "Running":
                                if (destination == null || duration < 0.0f)
                                {
                                    Debug.LogError("Cannot run, destination is null or duration is smaller than 0.");
                                }
                                else
                                {
                                    if (duration == 0.0f)
                                    {
                                        duration = Vector3.Distance((Vector3)destination, currentObject.transform.position) / RUNNING_SPEED;
                                    }
                                    relativeDirection = (Vector3)destination - currentObject.transform.position;
                                    MoveObject(objectName, (Vector3)destination, duration);
                                    RotateObject(objectName, relativeDirection, 0.2f); // rotate in 0.2 seconds
                                    currentObject.GetComponent<Animator>().Play("Running");
                                }
                                break;
                            default:
                                currentObject.GetComponent<Animator>().Play(animationName);
                                break;
                        }
                    }
                    break;
            }
        }
    }

    public bool IsPlaying()
    {
        return isPlaying;
    }

    public void Play()
    {
        Animator[] objectsWithAnimators = (Animator[])GameObject.FindObjectsOfType(typeof(Animator));
        foreach(Animator currentAnimator in objectsWithAnimators)
        {
            currentAnimator.speed = 1;
        }
        isPlaying = true;
    }

    public void Pause()
    {
        Animator[] objectsWithAnimators = (Animator[])GameObject.FindObjectsOfType(typeof(Animator));
        foreach (Animator currentAnimator in objectsWithAnimators)
        {
            currentAnimator.speed = 0;
        }
        isPlaying = false;
    }

    public void Discard()
    {
        // remove all pending animations from lists
        movingObjects.Clear();
        rotatingObjects.Clear();
    }
}

class MovableObject
{
    public GameObject gameObject;
    public Vector3 origin;
    public Vector3 destination;
    public float duration;
    public float currentLerpTime;
    public float progress;
}

class RotatableObject
{
    public GameObject gameObject;
    public Quaternion fromRotation;
    public Quaternion toRotation;
    public float duration;
    public float currentLerpTime;
    public float progress;
}