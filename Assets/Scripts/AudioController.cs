﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        if (source == null)
        {
            source = gameObject.AddComponent<AudioSource>();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TogglePause()
    {
        if (source.isPlaying)
        {
            source.Pause();
        }
        else
        {
            source.UnPause();
        }
    }

    public void LoadAudio(string filePath)
    {
        WWW www = new WWW("file:///" + filePath);
        source.clip = www.GetAudioClip();
        while (!source.isPlaying)
        {
            // play audio after file is properly loaded
            if (source.clip.isReadyToPlay)
            {
                source.Play();
            }
        }
    }
}
