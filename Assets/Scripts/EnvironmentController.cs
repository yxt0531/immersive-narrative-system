﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnvironmentController : MonoBehaviour
{
    private const float EXPOSURE_NOON = 1.75f;
    private const float EXPOSURE_NIGHT = 0.5f;

    private const float SKY_R_NOON = 1.0f;
    private const float SKY_G_NOON = 1.0f;
    private const float SKY_B_NOON = 1.0f;
    private const float SKY_R_NIGHT = 0.0f;
    private const float SKY_G_NIGHT = 0.0f;
    private const float SKY_B_NIGHT = 0.0f;

    private const float GROUND_R_NOON = 0.5f;
    private const float GROUND_G_NOON = 0.5f;
    private const float GROUND_B_NOON = 0.5f;
    private const float GROUND_R_NIGHT = 0.0f;
    private const float GROUND_G_NIGHT = 0.0f;
    private const float GROUND_B_NIGHT = 0.0f;

    private float timeEnd;
    private float timeNow;

    private bool isFading;
    private bool isFadingIn;

    private Color fadingColor;

    void Start()
    {
        SetDefault();
        timeEnd = 0.0f;
        timeNow = 0.0f;

        isFading = false;
        isFadingIn = false;
        fadingColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        GameObject.Find("EC_FadePlane").GetComponent<Renderer>().material.SetColor("_Color", fadingColor);
    }

    void Update()
    {
        if (isFading && timeNow <= timeEnd)
        {
            timeNow = timeNow + Time.deltaTime;
            
            if (isFadingIn)
            {
                fadingColor.a = 1.0f - Mathf.Lerp(0, 1, timeNow / timeEnd);
            }
            else
            {
                fadingColor.a = Mathf.Lerp(0, 1, timeNow / timeEnd);
            }

            GameObject.Find("EC_FadePlane").GetComponent<Renderer>().material.SetColor("_Color", fadingColor);
        }
    }

    public void SetDefault()
    {
        SetSunSizeConvergence(7.5f);
        SetSunSize(0.0f);

        SetSkyTintColor(1.0f, 1.0f, 1.0f, 1.0f);
        SetGroundColor(0.5f, 0.5f, 0.5f, 1.0f);

        SetExposure(1.75f);
        SetAtmosphereThickness(0.5f);
    }

    public void FadeIn(float duration, float r = 1.0f, float g = 1.0f, float b = 1.0f)
    {
        fadingColor.r = r;
        fadingColor.g = g;
        fadingColor.b = b;
        fadingColor.a = 1.0f;
        GameObject.Find("EC_FadePlane").GetComponent<Renderer>().material.SetColor("_Color", fadingColor);

        timeEnd = duration;
        timeNow = 0.0f;

        isFading = true;
        isFadingIn = true;
    }

    public void FadeOut(float duration, float r = 1.0f, float g = 1.0f, float b = 1.0f)
    {
        fadingColor.r = r;
        fadingColor.g = g;
        fadingColor.b = b;
        fadingColor.a = 0.0f;
        GameObject.Find("EC_FadePlane").GetComponent<Renderer>().material.SetColor("_Color", fadingColor);

        timeEnd = duration;
        timeNow = 0.0f;

        isFading = true;
        isFadingIn = false;
    }

    public void SetSunSize(float size)
    {
        if (size < 0)
        {
            RenderSettings.skybox.SetFloat("_SunSize", 0.0f);
        }
        else if (size > 1)
        {
            RenderSettings.skybox.SetFloat("_SunSize", 1.0f);
        }
        else
        {
            RenderSettings.skybox.SetFloat("_SunSize", size);
        }
    }

    public void SetSunSizeConvergence(float convergence)
    {
        if (convergence < 1)
        {
            RenderSettings.skybox.SetFloat("_SunSizeConvergence", 1.0f);
        }
        else if (convergence > 10)
        {
            RenderSettings.skybox.SetFloat("_SunSizeConvergence", 10.0f);
        }
        else
        {
            RenderSettings.skybox.SetFloat("_SunSizeConvergence", convergence);
        }
    }

    public void SetSkyTintColor(float r, float g, float b, float a)
    {
        RenderSettings.skybox.SetColor("_SkyTint", new Color(r, g, b, a));
    }

    public void SetGroundColor(float r, float g, float b, float a)
    {
        RenderSettings.skybox.SetColor("_GroundColor", new Color(r, g, b, a));
    }

    public void SetExposure(float exposure)
    {
        if (exposure < 0)
        {
            RenderSettings.skybox.SetFloat("_Exposure", 0.0f);
        }
        else if (exposure > 8)
        {
            RenderSettings.skybox.SetFloat("_Exposure", 8.0f);
        }
        else
        {
            RenderSettings.skybox.SetFloat("_Exposure", exposure);
        }
    }

    public void SetAtmosphereThickness(float thickness)
    {
        if (thickness < 0)
        {
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", 0.0f);
        }
        else if (thickness > 5)
        {
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", 5.0f);
        }
        else
        {
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", thickness);
        }
    }

    public void ChangeEnvironmentByTime(float time)
    {
        SetExposure(GetRealValueByTime(time, EXPOSURE_NIGHT, EXPOSURE_NOON));
        SetSkyTintColor(GetRealValueByTime(time, SKY_R_NIGHT, SKY_R_NOON), 
            GetRealValueByTime(time, SKY_G_NIGHT, SKY_G_NOON), 
            GetRealValueByTime(time, SKY_B_NIGHT, SKY_B_NOON), 
            1.0f);
        SetGroundColor(GetRealValueByTime(time, GROUND_R_NIGHT, GROUND_R_NOON),
            GetRealValueByTime(time, GROUND_G_NIGHT, GROUND_G_NOON),
            GetRealValueByTime(time, GROUND_B_NIGHT, GROUND_B_NOON),
            1.0f);
        GameObject.Find("/Directional Light").GetComponent<Light>().intensity = GetRealValueByTime(time, 0.0f, 1.25f);

        if (time > 24f)
        {
            time = 23.95f;
        }
        else if (time < 0)
        {
            time = 0.01f;
        }

        if (time < 13)
        {
            GameObject.Find("/Directional Light").transform.rotation = Quaternion.Euler(GetRealValueByTime(time, 0.0f, 85f), 130, 90);
        }
        else
        {
            GameObject.Find("/Directional Light").transform.rotation = Quaternion.Euler(GetRealValueByTime(time, 0.0f, 85f), 40, 90);
        }
        
    }

    private float GetRealValueByTime(float time, float min, float max)
    {
        // normalize time and return real value for environment control
        if (min <= 0)
        {
            min = 0.001f;
        }

        float timeNormalized;
        float multiplier;
        if (time > 23)
        {
            timeNormalized = 2.0f;
        }
        else if (time < 0)
        {
            timeNormalized = 0.0f;
        }
        else
        {
            timeNormalized = time / 12;
        }

        if (timeNormalized > 1.0f)
        {
            timeNormalized = (-timeNormalized + 2);
        }
        multiplier = ((max / min) * timeNormalized);
        multiplier = ((multiplier - 0) / (max / min));
        return ((max - min) * multiplier) + min;
    }
}
