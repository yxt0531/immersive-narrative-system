﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class VRSubController : MonoBehaviour
{
    private TextMeshPro subtitle;
    private float countdown;

    // Start is called before the first frame update
    void Start()
    {
        subtitle = GetComponent<TextMeshPro>();
        subtitle.text = "";
        countdown = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (countdown > 0)
        {
            countdown = countdown - UnityEngine.Time.deltaTime;
        }
        else
        {
            subtitle.text = "";
        }
    }

    public void ShowSubtitle(string text, float duration = 2.0f)
    {
        countdown = duration;
        subtitle.text = text;
    }
}
