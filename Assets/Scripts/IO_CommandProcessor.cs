﻿using DxR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IO_CommandProcessor : MonoBehaviour
{
    private string path = null; // store absolute path for current loaded datafile.txt
    private string dir = null;

    private Dictionary<string, GameObject> actorList = new Dictionary<string, GameObject>();

    private bool playing = false;
    private float startTime = -1;
    private Stopwatch stopwatch = new Stopwatch();

    // TODO add all future corrsponding prefabs here
    private AnimationController animationController;
    private AudioController audioController;
    private GLTF gltfController;
    private VRSubController vrSubController;
    private EnvironmentController envController;

    public GameObject DxRVis;

    void Start()
    {
        // TODO add all future corrsponding prefab references here
        try
        {
            animationController = GameObject.Find("AnimationController").GetComponent<AnimationController>();
        }
        catch (NullReferenceException e)
        {
            UnityEngine.Debug.LogError("Unable to load animation controller, please place animation controller prefab under root of current scene.");
        }

        try
        {
            audioController = GameObject.Find("AudioController").GetComponent<AudioController>();
        }
        catch (NullReferenceException e)
        {
            UnityEngine.Debug.LogError("Unable to load audio controller, please place audio controller prefab under root of current scene.");
        }

        try
        {
            gltfController = GameObject.Find("GLTF").GetComponent<GLTF>();
        }
        catch (NullReferenceException e)
        {
            UnityEngine.Debug.LogError("Unable to load GLTF controller, please place GLTF controller prefab under root of current scene.");
        }

        try
        {
            vrSubController = GameObject.Find("VRSubController").GetComponent<VRSubController>();
        }
        catch (NullReferenceException e)
        {
            UnityEngine.Debug.LogError("Unable to load VR subtitle controller, please place VR subtitle controller prefab under VR camera of current scene.");
        }

        try
        {
            envController = GameObject.Find("EnvironmentController").GetComponent<EnvironmentController>();
        }
        catch (NullReferenceException e)
        {
            UnityEngine.Debug.LogError("Unable to load environment controller, please place environment controller prefab under VR camera of current scene.");
        }
    }

    void Update()
    {

    }

    public void SetPath(string path)
    {
        this.path = path;
        dir = System.IO.Path.GetDirectoryName(path) + "/";
        UnityEngine.Debug.Log("Current datafile: " + path);
        UnityEngine.Debug.Log("Current working directory: " + dir);
    }

    public void ClearScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void TogglePause()
    {
        if (playing)
        {
            Time.timeScale = 0;
            playing = false;
            audioController.TogglePause();
        }
        else
        {
            Time.timeScale = 1;
            playing = true;
            audioController.TogglePause();
        }
    }

    public bool IsPlaying()
    {
        return playing;
    }

    public void process(List<string> arguments)
    {

        if (arguments == null)
        {
            return;
        }

        float time = -1;
        string actor = "";
        string action = "";
        int argStart = 0;
        bool errorOut = false;

        foreach (string argument in arguments)
        {
            if (actor != "" && action != "")
            {
                break;
            }
            else if (argument[0] == '[')
            {
                if (time == -1)
                {
                    time = float.Parse(argument.Substring(1, 2)) * 60 + float.Parse(argument.Substring(4, 2));
                    argStart++;                   
                }
                else
                {
                    UnityEngine.Debug.LogError("Error: Duplicate Time");
                }
                continue;
                
            }
            switch (argument)
            {
                case "CREATE":
                    actor = "SYSTEM";
                    action = "CREATE";
                    argStart++;
                    break;
                case "WAIT":
                    actor = "SYSTEM";
                    action = "WAIT";
                    argStart++;
                    break;
                case "PLAY_SOUND":
                    actor = "SYSTEM";
                    action = "PLAY_SOUND";
                    argStart++;
                    break;
                case "SUBTITLE":
                    actor = "SYSTEM";
                    action = "SUBTITLE";
                    argStart++;
                    break;

                case "FADEIN":
                    actor = "SYSTEM";
                    action = "FADEIN";
                    argStart++;
                    break;
                case "FADEOUT":
                    actor = "SYSTEM";
                    action = "FADEOUT";
                    argStart++;
                    break;

                case "SET_TIMEOFDAY":
                    actor = "SYSTEM";
                    action = "SET_TIMEOFDAY";
                    argStart++;
                    break;

                // TODO other system commands go here
                default:
                    if (actor == "")
                    {
                        actor = argument;
                    }
                    else
                    {
                        action = argument;
                    }
                    argStart++;
                    break;
            }
        }

        if (actor == "" || action == "")
        {
            UnityEngine.Debug.LogError("Error: Insufficient Arguments");
        }
        else
        {
            UnityEngine.Debug.Log(actor + " " + action);

            float waitTime = time - (stopwatch.ElapsedMilliseconds / 1000);
            if (waitTime < 0)
            {
                waitTime = 0;
            }

            playing = true;
            StartCoroutine(Waiter(actor, action, arguments, argStart, waitTime));
        }
    }

    IEnumerator Waiter(string actor, string action, List<string> arguments, int argStart, float waitTime)
    {
        while (!playing)
        {
            yield return null;
        }

        yield return new WaitForSeconds(waitTime);

        if (actor == "SCENE")
        {
            switch (action)
            {
                case "START":
                    stopwatch.Reset();
                    UnityEngine.Debug.Log("Scene Start");
                    break;
                case "PLAY":
                    stopwatch.Start();
                    UnityEngine.Debug.Log("Scene Play");
                    break;
                case "END":
                    stopwatch.Stop();
                    UnityEngine.Debug.Log("Scene End after " + stopwatch.Elapsed);
                    // clear scene
                    break;
                default:
                    UnityEngine.Debug.LogError("Error: Unknown Command for Scene: " + action);
                    break;
            }
        }
        else if (actor == "SYSTEM")
        {
            switch (action)
            {
                // this command places an object in the world.
                case "CREATE":
                    string coords = arguments[argStart + 1];
                    Vector3 position = PositionRectifier(coords);

                    if (arguments[argStart] == "DATAVIS" && arguments.Count - argStart == 5)
                    {
                        // TODO move this under Create method
                        CreateDataVisualisation(position, float.Parse(arguments[argStart + 2].Substring(1, arguments[argStart + 2].Length - 2)), arguments[argStart + 3], dir + TrimParentheses(arguments[argStart + 4]));
                    }
                    else if (arguments.Count - argStart == 4)
                    {
                        Create(arguments[argStart], position, float.Parse(arguments[argStart + 2].Substring(1, arguments[argStart + 2].Length - 2)), arguments[argStart + 3]);
                    }
                    else if (arguments.Count - argStart == 3)
                    {
                        Create(arguments[argStart], position, float.Parse(arguments[argStart + 2].Substring(1, arguments[argStart + 2].Length - 2)));
                    }
                    else if (arguments.Count - argStart == 6)
                    {
                        // for creating point light
                        Create(arguments[argStart], position, 0.0f, arguments[argStart + 5], arguments[argStart + 2], arguments[argStart + 3], arguments[argStart + 4]);
                    }
                    else if (arguments.Count - argStart == 2)
                    {
                        // for creating teleport point
                        Create(arguments[argStart], position);
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("Cannot Create Object, Invalid Arguments Number: " + (arguments.Count - argStart));
                    }

                    break;

                case "WAIT":
                    break; // this command stops the file from reaching the scene end

                case "PLAY_SOUND":
                    if (arguments.Count == 3)
                    {
                        if (IsFileExist(dir + TrimParentheses(arguments[argStart])))
                        {
                            audioController.LoadAudio(dir + TrimParentheses(arguments[argStart]));
                        }
                        
                        // UnityEngine.Debug.Log("Play Sound File: " + dir + arguments[argStart]);
                    }
                    break;

                case "SUBTITLE":
                    if (arguments.Count >= 3)
                    {
                        string sub = "";
                        float duration;

                        for (int i = argStart; i < arguments.Count - 1; i++)
                        {
                            sub = sub + arguments[i] + " "; // chain arguments together as one string to use as subtitle
                        }
                        sub = sub.Substring(1, sub.Length - 3); // trim parentheses from sub string
                        duration = float.Parse(arguments[arguments.Count - 1].Substring(1, arguments[arguments.Count - 1].Length - 2));

                        vrSubController.ShowSubtitle(sub, duration);
                    }
                    break;


                case "FADEIN":
                    if (arguments.Count == 3)
                    {
                        // without color
                        envController.FadeIn(float.Parse(arguments[arguments.Count - 1].Substring(1, arguments[arguments.Count - 1].Length - 2)));
                    }
                    else if (arguments.Count == 4)
                    {
                        // with color
                        envController.FadeIn(float.Parse(arguments[arguments.Count - 1].Substring(1, arguments[arguments.Count - 1].Length - 2)),
                            ColorRectifier(arguments[2]).r,
                            ColorRectifier(arguments[2]).g,
                            ColorRectifier(arguments[2]).b);
                    }
                    break;

                case "FADEOUT":
                    if (arguments.Count == 3)
                    {
                        // without color
                        envController.FadeOut(float.Parse(arguments[arguments.Count - 1].Substring(1, arguments[arguments.Count - 1].Length - 2)));
                    }
                    else if (arguments.Count == 4)
                    {
                        // with color
                        envController.FadeOut(float.Parse(arguments[arguments.Count - 1].Substring(1, arguments[arguments.Count - 1].Length - 2)),
                            ColorRectifier(arguments[2]).r,
                            ColorRectifier(arguments[2]).g,
                            ColorRectifier(arguments[2]).b);
                    }

                    break;


                case "SET_TIMEOFDAY":
                    if (arguments.Count == 3)
                    {
                        envController.ChangeEnvironmentByTime(float.Parse(arguments[arguments.Count - 1].Substring(1, arguments[arguments.Count - 1].Length - 2)));
                    }
                    break;


                default:
                    UnityEngine.Debug.LogError("Error: Unknown Command for System Action: " + action);
                    break;
            }
        }
        else
        {
            switch (action)
            {
                case "MOVE":
                    Vector3 position = PositionRectifier(arguments[argStart]);
                    float rotationDegree = float.Parse(arguments[argStart + 1].Substring(1, arguments[argStart + 1].Length - 2));
                    Vector3 rotationVector = new Vector3(0, rotationDegree, 0);
                    Quaternion rotation = Quaternion.Euler(rotationVector); // convert degree to quaternion around y-axis
                    float duration = float.Parse(arguments[argStart + 2].Substring(1, arguments[argStart + 2].Length - 2));

                    animationController.MoveObject(actor, position, duration);
                    animationController.RotateObject(actor, rotation, duration);

                    break;
                case "PLAY_ANIMATION":
                    // play object animation
                    try 
                    {
                        animationController.PlayAnimation(actor, arguments[argStart]); // play specified animation
                    }
                    catch (Exception e)
                    {
                        animationController.PlayAnimation(actor, null); // no animation name specified, play default animation if any
                    }
                    break;

                case "HIDE":
                    GameObject.Find(actor).transform.localScale = new Vector3(0.0f, 0.0f, 0.0f);
                    GameObject.Find(actor).transform.position = GameObject.Find(actor).transform.position + new Vector3(5000f, 5000f, 5000f); // temp solution
                    break;

                case "SHOW":
                    GameObject.Find(actor).transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    GameObject.Find(actor).transform.position = GameObject.Find(actor).transform.position - new Vector3(5000f, 5000f, 5000f); // temp solution
                    break;

                case "WALKTO":
                    try
                    {
                        animationController.PlayAnimation(actor, "Walking", PositionRectifier(arguments[argStart])); // play walking animation
                    }
                    catch (Exception e)
                    {
                        
                    }
                    break;

                case "RUNTO":
                    try
                    {
                        animationController.PlayAnimation(actor, "Running", PositionRectifier(arguments[argStart])); // play running animation
                    }
                    catch (Exception e)
                    {

                    }
                    break;

                default:
                    UnityEngine.Debug.LogError("Error: Unknown Command for Action: " + action);
                    break;
            }
        }
    }

    bool Create(string newObject, Vector3 position, float rotation = 0.0f, string name = "", string lightColor = "(1.0,0.0,0.0)", string lightIntensity = "(1)", string lightRange = "(10)")
    {
        GameObject createdObject;
        switch (newObject)
        {
            case "TELEPOINT":
                createdObject = Instantiate(Resources.Load("TeleportPoint") as GameObject, position, Quaternion.Euler(0, rotation, 0));
                createdObject.tag = "Static"; // all teleport
                createdObject.name = name;
                actorList.Add(name, createdObject);
                break;

            case "LIGHT":
                createdObject = new GameObject(name);
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                createdObject.transform.position = position;
                createdObject.transform.rotation = Quaternion.Euler(0, rotation, 0);

                Light lightComp = createdObject.AddComponent<Light>();
                lightComp.color = ColorRectifier(lightColor);
                lightComp.intensity = float.Parse(TrimParentheses(lightIntensity));
                lightComp.range = float.Parse(TrimParentheses(lightRange));

                break;

            case "SPHERE":

                createdObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                createdObject.transform.position = position;
                createdObject.transform.rotation = Quaternion.Euler(0, rotation, 0);
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                UnityEngine.Debug.Log("Creating Object: Sphere");

                return true;

            case "PLANE":

                createdObject = GameObject.CreatePrimitive(PrimitiveType.Plane);
                createdObject.transform.position = position;
                createdObject.transform.rotation = Quaternion.Euler(0, rotation, 0);
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                UnityEngine.Debug.Log("Creating Object: Plane");

                return true;

            case "CUBE":

                createdObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
                createdObject.transform.position = position;
                createdObject.transform.rotation = Quaternion.Euler(0, rotation, 0);
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                UnityEngine.Debug.Log("Creating Object: Cube");

                return true;

            case "PERSON":
                createdObject = Instantiate(Resources.Load("Person_1") as GameObject, position, Quaternion.Euler(0, rotation, 0));
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                UnityEngine.Debug.Log("Creating Person: " + name);

                return true;

            case "CYCLIST":
                createdObject = Instantiate(Resources.Load("Cyclist_1") as GameObject, position, Quaternion.Euler(0, rotation, 0));
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                UnityEngine.Debug.Log("Creating Person: " + name);

                return true;

            case "LUKA":
                createdObject = Instantiate(Resources.Load("White_Sour_Luka") as GameObject, position, Quaternion.Euler(0, rotation, 0));
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                return true;

            case "LUKA_BLACK":
                createdObject = Instantiate(Resources.Load("Black_Sour_Luka") as GameObject, position, Quaternion.Euler(0, rotation, 0));
                createdObject.name = name;
                if (name != "")
                {
                    actorList.Add(name, createdObject);
                    createdObject.tag = "Kinematic"; // named object will be Static
                }
                else
                {
                    createdObject.tag = "Static";
                    // unnamed object will be Static
                }
                return true;

            default:
                newObject = TrimParentheses(newObject);
                gltfController.LoadFile(name, dir + newObject);
                if (string.IsNullOrEmpty(name)) // use file name as object name to set translation and rotation
                {
                    createdObject = GameObject.Find(newObject);
                    createdObject.transform.position = position;
                    createdObject.transform.rotation = Quaternion.Euler(0, rotation, 0);
                }
                else
                {
                    createdObject = GameObject.Find(name);
                    createdObject.transform.position = position;
                    createdObject.transform.rotation = Quaternion.Euler(0, rotation, 0);
                }
                break;
        }

        return false;
    }

    void CreateDataVisualisation(Vector3 position, float rotation, string name, string path)
    {
        DxRVis.GetComponent<Vis>().visSpecsURL = path;
        GameObject createdObject = Instantiate(DxRVis, position, Quaternion.Euler(0, rotation, 0));
        createdObject.name = name;
        createdObject.GetComponent<Vis>().UpdateVis();
    }

    Vector3 PositionRectifier(string stringPos)
    {
        float xPos = float.Parse(stringPos.Substring(1, stringPos.IndexOf(',') - 1));
        stringPos = stringPos.Substring(stringPos.IndexOf(',') + 1);
        float yPos = float.Parse(stringPos.Substring(0, stringPos.IndexOf(',')));
        stringPos = stringPos.Substring(stringPos.IndexOf(',') + 1);
        float zPos = float.Parse(stringPos.Substring(0, stringPos.Length - 1));

        Vector3 returnVector = new Vector3(xPos, yPos, zPos);

        return returnVector;
    }

    Color ColorRectifier(string stringColor)
    {
        // UnityEngine.Debug.Log("R:" + stringColor.Substring(1, stringColor.IndexOf(',') - 1));
        float r = float.Parse(stringColor.Substring(1, stringColor.IndexOf(',') - 1));

        stringColor = stringColor.Substring(stringColor.IndexOf(',') + 1);
        // UnityEngine.Debug.Log("G:" + stringColor.Substring(0, stringColor.IndexOf(',')));
        float g = float.Parse(stringColor.Substring(0, stringColor.IndexOf(',')));

        stringColor = stringColor.Substring(stringColor.IndexOf(',') + 1);
        // UnityEngine.Debug.Log("B:" + stringColor.Substring(0, stringColor.Length - 1));
        float b = float.Parse(stringColor.Substring(0, stringColor.Length - 1));
        Color returnColor = new Color(r, g, b);

        return returnColor;
    }

    string TrimParentheses(string input)
    {
        return input.Substring(input.IndexOf('(') + 1, input.LastIndexOf(')') - 1); // trim parentheses from sub string
    }

    bool IsFileExist(string path)
    {
        if (System.IO.File.Exists(path))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
