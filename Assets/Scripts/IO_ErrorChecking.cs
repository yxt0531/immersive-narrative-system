﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using UnityEngine;


public class IO_ErrorChecking : MonoBehaviour
{
    public bool ValidateFile(string filePath)
    {
        bool noError = true;

        try
        {
            //Name different types of lists
            List<string> lines = new List<string>();
            List<string> objectList = new List<string>();
            List<string> actionList = new List<string>();
            List<string> errorLineList = new List<string>();


            //ActionList population
            actionList.Add("MOVE");
            actionList.Add("WALK");
            actionList.Add("RUN");


            //file path section           
            string line = "";
            int counter = 1;

            System.IO.StreamReader file = new System.IO.StreamReader(filePath);

            //readLine if !null
            while ((line = file.ReadLine()) != null)
            {
                UnityEngine.Debug.Log(line);
                lines.Add(line);
                int i = 0;
                string[] arrayStr = line.Split(' ');
                //base array Length set up different Lines.
                if (i < arrayStr.Length)
                {
                    if (arrayStr[0] == "SCENE")
                    {
                        switch (arrayStr[1])
                        {
                            case "BEGIN":
                                counter++;
                                break;
                            case "PLAY":
                                counter++;
                                break;
                            case "END":
                                counter++;
                                break;
                            default:
                                UnityEngine.Debug.Log("Error on line: " + counter);
                                counter++;
                                noError = false;
                                break;
                        }
                    }
                  
                    //CREATE setting
                    else if (arrayStr[0] == "CREATE")
                    {
                        //if create string is PERSON
                        if (arrayStr[1] == "PERSON" || arrayStr[1] == "CUBE" || arrayStr[1] == "SPHERE" || arrayStr[1] == "PLANE")
                        {
                            //get left and right bracket or position, rotation and scale 
                            string leftPos = arrayStr[2].Substring(0, 1);
                            string rightPos = arrayStr[2].Substring(arrayStr[2].Length - 1);
                            string[] posStr = arrayStr[2].Split(',');
                            string leftRot = arrayStr[3].Substring(0, 1);
                            string rightRot = arrayStr[3].Substring(arrayStr[3].Length - 1);
                            
                            //if two bracket correct
                            if (leftPos == "(" && rightPos == ")")
                            {

                            }
                            else
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                            //position x
                            try
                            {
                                string xpos = posStr[0].Substring(1);
                                Convert.ToDouble(xpos);
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.Log("Not A number ");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                            //position y
                            try
                            {
                                string ypos = posStr[1];
                                Convert.ToDouble(ypos);
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.Log("Not A number ");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                            //position z
                            try
                            {
                                string zpos = posStr[2].Substring(0, 1);   //(0,0,0)
                                Convert.ToDouble(zpos);
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.Log("Not A number ");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }

                            //if two bracket correct
                            if (leftRot == "(" && rightRot == ")")
                            {

                            }
                            else
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                            //rotation check
                            try
                            {
                                string rot = arrayStr[3].Substring(1, arrayStr[3].Length - 2);
                                Convert.ToDouble(rot);
                            }
                            catch (Exception e)
                            {
                                UnityEngine.Debug.Log("Not A number ");
                                UnityEngine.Debug.Log(e.Message);
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }

                            //add person name to list
                            if (arrayStr.Length == 5)
                            {
                                try
                                {
                                    bool duplicate = false;
                                    foreach (string name in objectList)
                                    {
                                        if (name == arrayStr[4])
                                        {
                                            duplicate = true;
                                        }
                                    }

                                    if (!duplicate)
                                    {
                                        objectList.Add(arrayStr[4]);
                                    }
                                    else
                                    {
                                        UnityEngine.Debug.Log("Error on line: " + counter + ". Object with that name already exists");
                                        noError = false;
                                    }
                                }
                                catch (Exception e)
                                {
                                    UnityEngine.Debug.Log(e.Message);
                                    noError = false;
                                }                               
                            }                                                     
                        }                      
                        else
                        {
                            UnityEngine.Debug.Log("Possible undefined object on line " + counter + " !");
                            errorLineList.Add(Convert.ToString(counter));
                            noError = false;
                        }
                    }
                    
                    //time stamp checking , check first character is [
                    else if (arrayStr[0].Substring(0, 1) == "[")
                    {
                        //time stap setting
                        string[] timeStr = arrayStr[0].Split(':');
                        string leftBracket = arrayStr[0].Substring(0, 1);
                        string rightBracket = arrayStr[0].Substring(arrayStr[0].Length - 1);
                        //mins set
                        try
                        {
                            string mins = timeStr[0].Substring(1);
                            int min = Convert.ToInt16(mins);
                            if (min < 0)
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                            else if (min > 60)
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                        }
                        catch (Exception e)
                        {
                            UnityEngine.Debug.Log("Not A number ");
                            errorLineList.Add(Convert.ToString(counter));
                            noError = false;
                        }

                        //seconds set
                        try
                        {
                            string secs = timeStr[1].Substring(0, timeStr.Length - 1);
                            int sec = Convert.ToInt16(secs);
                            if (sec < 0)
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                            else if (sec > 60)
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                        }
                        catch (Exception e)
                        {
                            UnityEngine.Debug.Log("Not A number ");
                            errorLineList.Add(Convert.ToString(counter));
                            noError = false;
                        }

                        //check if object is in list
                        if (objectList.Contains(arrayStr[1]))
                        {
                            //check if action is in list 
                            if (actionList.Contains(arrayStr[2]))
                            {
                                //position setting
                                string leftPos = arrayStr[3].Substring(0, 1);
                                string rightPos = arrayStr[3].Substring(arrayStr[3].Length - 1);
                                string[] posStr = arrayStr[3].Split(',');
                                if (leftPos == "(" && rightPos == ")")
                                {

                                }
                                else
                                {
                                    UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                    errorLineList.Add(Convert.ToString(counter));
                                }
                                try
                                {
                                    string xpos = posStr[0].Substring(1);
                                    Convert.ToDouble(xpos);
                                }
                                catch (Exception e)
                                {
                                    UnityEngine.Debug.Log("Not A number ");
                                    errorLineList.Add(Convert.ToString(counter));
                                    noError = false;
                                }
                                try
                                {
                                    string ypos = posStr[1];
                                    Convert.ToDouble(ypos);
                                }
                                catch (Exception e)
                                {
                                    UnityEngine.Debug.Log("Not A number ");
                                    errorLineList.Add(Convert.ToString(counter));
                                    noError = false;
                                }
                                try
                                {
                                    string zpos = posStr[2].Substring(0, 1);   //(0,0,0)
                                    Convert.ToDouble(zpos);
                                }
                                catch (Exception e)
                                {
                                    UnityEngine.Debug.Log("Not A number ");
                                    errorLineList.Add(Convert.ToString(counter));
                                    noError = false;
                                }
                            }
                            else
                            {
                                UnityEngine.Debug.Log("Error on line: " + counter + " !");
                                errorLineList.Add(Convert.ToString(counter));
                                noError = false;
                            }
                        }
                        else
                        {
                            UnityEngine.Debug.Log("Error on line: " + counter + " !");
                            errorLineList.Add(Convert.ToString(counter));
                            noError = false;
                        }
                    }
                    else
                    {
                        UnityEngine.Debug.Log("Error on line: " + counter + " !");
                        errorLineList.Add(Convert.ToString(counter));
                        noError = false;
                    }
                }
                counter++;
            }
        }
        //if file cannot read.
        catch (Exception e)
        {
            UnityEngine.Debug.Log("The file could not be read:");
            UnityEngine.Debug.Log(e.Message);
            //System.Console.ReadKey();
            noError = false;
        }

        return noError;
    }
}

