﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IO_Loader : MonoBehaviour
{
    private string line = null;
    private string segment = "";
    private System.IO.StreamReader file;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    { 
        
    }

    public void setFilePath(string filePath)
    {
        file = new System.IO.StreamReader(filePath);
        line = file.ReadLine();
    }

    public List<string> nextLine()
    {
        if (line != null)
        {
            int breakCounter = 0;
            List<string> arguments = new List<string>();

            while (true)
            {
                // break off the first word               
                int position = line.IndexOf(" ");
                if (position > -1)
                {
                    segment = line.Substring(0, position);
                    line = line.Substring(position + 1);
                }
                else
                {
                    segment = line.Substring(0, line.Length);
                    line = "";
                }

                // file the segment info
                if (segment != "")
                {
                    arguments.Add(segment);
                }

                // new line break
                if (line == "")
                {
                    break;
                }

                breakCounter++;
                if (breakCounter > 100)
                {
                    UnityEngine.Debug.Log("Emergency Break");
                    break;
                }
            }

            // next line
            line = file.ReadLine();

            if (line == null)
            {
                file.Close();
            }

            return arguments;          
        }

        return null;

    }
    
}



