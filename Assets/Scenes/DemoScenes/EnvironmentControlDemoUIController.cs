﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnvironmentControlDemoUIController : MonoBehaviour
{
    private EnvironmentController envController;


    // Start is called before the first frame update
    void Start()
    {
        envController = new EnvironmentController();
        Reset();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeSunSize()
    {
        envController.SetSunSize(GameObject.Find("/Canvas/SunSize").GetComponent<Slider>().value);
    }

    public void ChangeSunSizeConvergence()
    {
        envController.SetSunSizeConvergence(GameObject.Find("/Canvas/SunSizeConvergence").GetComponent<Slider>().value);
    }

    public void ChangeExposure()
    {
        envController.SetExposure(GameObject.Find("/Canvas/Exposure").GetComponent<Slider>().value);
    }

    public void ChangeAtmosphereThickness()
    {
        envController.SetAtmosphereThickness(GameObject.Find("/Canvas/AtmosphereThickness").GetComponent<Slider>().value);
    }

    public void ChangeColor()
    {
        envController.SetSkyTintColor(GameObject.Find("/Canvas/SkyR").GetComponent<Slider>().value,
            GameObject.Find("/Canvas/SkyG").GetComponent<Slider>().value,
            GameObject.Find("/Canvas/SkyB").GetComponent<Slider>().value, 1.0f);
    }

    public void ChangeGroundColor()
    {
        envController.SetGroundColor(GameObject.Find("/Canvas/GroundR").GetComponent<Slider>().value,
            GameObject.Find("/Canvas/GroundG").GetComponent<Slider>().value,
            GameObject.Find("/Canvas/GroundB").GetComponent<Slider>().value, 1.0f);
    }

    public void ChangeTime()
    {
        envController.ChangeEnvironmentByTime((int)GameObject.Find("/Canvas/Time").GetComponent<Slider>().value);
    }

    public void Reset()
    {
        envController.SetDefault();
    }
}
