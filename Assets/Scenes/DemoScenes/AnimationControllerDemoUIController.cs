﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AnimationControllerDemoUIController : MonoBehaviour
{
    private AnimationController animationController;
    private GLTF gltfController;
    // Start is called before the first frame update
    void Start()
    {
        animationController = GameObject.Find("AnimationController").GetComponent<AnimationController>();
        gltfController = GameObject.Find("GLTF").GetComponent<GLTF>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TogglePlayButton()
    {
        if (animationController.IsPlaying())
        {
            GameObject.Find("/Canvas/PlayButton/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("Play");
            animationController.Pause();
        }
        else
        {
            GameObject.Find("/Canvas/PlayButton/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("Pause");
            animationController.Play();
        }
    }

    public void TranslateObject()
    {
        animationController.MoveObject(
            GameObject.Find("/Canvas/ObjectName/Text").GetComponent<Text>().text,
            new Vector3(
                float.Parse(GameObject.Find("/Canvas/X/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Y/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Z/Text").GetComponent<Text>().text)
                ),
            float.Parse(GameObject.Find("/Canvas/Duration/Text").GetComponent<Text>().text)
            );
    }

    public void RotateObject()
    {
        animationController.RotateObject(
            GameObject.Find("/Canvas/ObjectName/Text").GetComponent<Text>().text,
            new Vector3(
                float.Parse(GameObject.Find("/Canvas/X/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Y/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Z/Text").GetComponent<Text>().text)
                ),
            float.Parse(GameObject.Find("/Canvas/Duration/Text").GetComponent<Text>().text)
            );
    }

    public void WalkTo()
    {
        animationController.PlayAnimation(
            GameObject.Find("/Canvas/ObjectName/Text").GetComponent<Text>().text,
            "Walking",
            new Vector3(
                float.Parse(GameObject.Find("/Canvas/X/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Y/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Z/Text").GetComponent<Text>().text)
                ),
            float.Parse(GameObject.Find("/Canvas/Duration/Text").GetComponent<Text>().text)
            );
    }

    public void RunTo()
    {
        animationController.PlayAnimation(
            GameObject.Find("/Canvas/ObjectName/Text").GetComponent<Text>().text,
            "Running",
            new Vector3(
                float.Parse(GameObject.Find("/Canvas/X/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Y/Text").GetComponent<Text>().text),
                float.Parse(GameObject.Find("/Canvas/Z/Text").GetComponent<Text>().text)
                ),
            float.Parse(GameObject.Find("/Canvas/Duration/Text").GetComponent<Text>().text)
            );
    }

    public void LoadGLTF()
    {
        gltfController.LoadFile("GLTF_Imported", null);
    }
}
