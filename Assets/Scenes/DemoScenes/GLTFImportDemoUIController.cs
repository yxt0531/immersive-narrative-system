﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GLTFImportDemoUIController : MonoBehaviour
{
    private GLTF gltfController;
    // Start is called before the first frame update
    void Start()
    {
        gltfController = new GLTF();
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text = GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text;
    }

    public void Load()
    {
        gltfController.LoadFile(GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text, null);
        Debug.Log("Load Model As " + GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text);
    }

    public void RotateLeft()
    {
        gltfController.Rotate(GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text, new Vector3(0, -10, 0)); ;
    }

    public void RotateRight()
    {
        gltfController.Rotate(GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text, new Vector3(0, 10, 0)); ;
    }

    public void MoveLeft()
    {
        gltfController.Translate(GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text, new Vector3(0.2f, 0, 0)); ;
    }

    public void MoveRight()
    {
        gltfController.Translate(GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text, new Vector3(-.2f, 0, 0));
    }

    public void Play()
    {
        gltfController.PlayAnimation(GameObject.Find("/Canvas/InputField/Text").GetComponent<Text>().text, GameObject.Find("/Canvas/AnimationInputField/Text").GetComponent<Text>().text);
    }
}
