﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerSceneUIController : MonoBehaviour
{
    private IO_Controller ioController;

    // Start is called before the first frame update
    void Start()
    {
        ioController = GameObject.Find("IO").GetComponent<IO_Controller>();
        GameObject.Find("/Canvas/Pause").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/Reset").GetComponent<Button>().interactable = false;
        GameObject.Find("/Canvas/FFw").GetComponent<Button>().interactable = false;

        GameObject.Find("/Canvas/Pause").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("/Canvas/Reset").transform.localScale = new Vector3(0, 0, 0);
        GameObject.Find("/Canvas/FFw").transform.localScale = new Vector3(0, 0, 0);

        GameObject.Find("/Player").transform.position = new Vector3(0, 0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Load()
    {
        ioController.LoadFile();
        GameObject.Find("/Canvas/Pause").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/Reset").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/FFw").GetComponent<Button>().interactable = true;
        GameObject.Find("/Canvas/Load").SetActive(false);

        GameObject.Find("/Canvas/Pause").transform.localScale = new Vector3(1, 1, 1);
        GameObject.Find("/Canvas/Reset").transform.localScale = new Vector3(1, 1, 1);
        GameObject.Find("/Canvas/FFw").transform.localScale = new Vector3(1, 1, 1);
    }

    public void Pause()
    {
        if (ioController.IsPlaying())
        {
            GameObject.Find("/Canvas/Pause/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("Resume");
            GameObject.Find("/Canvas/FFw").GetComponent<Button>().interactable = false;
            ioController.TogglePause();
        }
        else
        {
            GameObject.Find("/Canvas/Pause/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("Pause");
            GameObject.Find("/Canvas/FFw").GetComponent<Button>().interactable = true;
            ioController.TogglePause();
        }
    }

    public void ClearScene()
    {
        GameObject.Find("/Player").transform.position = new Vector3(0, 0, 5); // reset player position
        Destroy(GameObject.Find("/Player"));
        ioController.ClearScene();
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus && ioController.IsPlaying())
        {
            Pause(); // auto pause when window is out of focus
        }
    }

    public void TogglePlaybackSpeed()
    {
        if (Time.timeScale == 0.0f)
        {
            return;
        }
        else if (Time.timeScale == 1.0f)
        {
            Time.timeScale = 2;
            GameObject.Find("/Canvas/FFw/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("x2");
        }
        else if (Time.timeScale == 2.0f)
        {
            Time.timeScale = 4;
            GameObject.Find("/Canvas/FFw/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("x4");
        } else
        {
            Time.timeScale = 1;
            GameObject.Find("/Canvas/FFw/Text (TMP)").GetComponent<TextMeshProUGUI>().SetText("Fast-Forward");
        }
    }

    public void QuitINS()
    {
        Application.Quit();
    }
}
