# Datafile Syntax

## Basic Structure

Syntax: SCENE START + *commands* + SCENE PLAY + [timeStamp] *commands* + SCENE END

All commands between **SCENE START** and **SCENE PLAY** will be executed before start of scene play.
All commands between **SCENE PLAY** and **SCENE END** will be executed according to its timestamp.
Commands between **SCENE PLAY** and **SCENE END** with timestamp [00:00] will have the same effect as they were in between **SCENE START** and **SCENE PLAY**.
For external resources such as GLB files and sound files, **avoid having parentheses** in their file names and corrsponding paths.

Create a cube and a plane at beginning, then create another cube named Orb at 00:02, a sphere named Ball at 00:03, a person named Tommy at 00:04, then move the cube named Orb to a new location at 00:05:

```INS
SCENE START
CREATE CUBE (0,1,0) (45)
CREATE PLANE (0,0,0) (0)
SCENE PLAY
[00:02] CREATE CUBE (2,2,2) (0) Orb
[00:03] CREATE SPHERE (0,5,0) (0) Ball
[00:04] CREATE PERSON (0,0,0) (90) Tommy
[00:05] Orb MOVE (-2,2,2) (180) (5)
SCENE END
```

## Object Creation

### Creating Model

Syntax: [timeStamp] CREATE *objectType* (position) (rotationInDegree) *objectName*

At 00:01, Create a cube named MyCube at positon (0, 2.5, 0) with rotation 0 degree:

```INS
[00:01] CREATE CUBE (0,2.5,0) (0) MyCube
```

Available objectType: CUBE, SPHERE, PLANE, PERSON, CYCLIST

Some of object types has advanced animation which can be played using *PLAY_ANIMATION*

PERSON: Idle, Walking, Running

CYCLIST: Idle, OnBicycle_Idle, RideLoop, Crash

### Creating Model using GLB file

Syntax: [timeStamp] CREATE (fileName) (position) (rotationInDegree) *objectName*

At 00:00, Using Squirrel.glb in my datafile root directory to create an object named Squirrel_Imported at positon (-5, 0, 0) with rotation 180 degree:

```INS
[00:00] CREATE (Squirrel.glb) (-5,0,0) (180) Squirrel_Imported
```

### Creating Light

Syntax: [timeStamp] CREATE LIGHT (position) (color) (intensity) (range) *lightName*

At 00:02, Create a pale blue light (RGB value: 0.5, 0.5, 1) named BlueLight at positon (2, 1, -2.5) with intensity 1 and range 5:

```INS
[00:02] CREATE LIGHT (2,1,-2.5) (0.5,0.5,1) (1) (5) BlueLight
```

### Creating Teleportation Point

Syntax: [timeStamp] CREATE TELEPOINT (position)

Create a teleportation point at position (0, 0, 5)

```INS
CREATE TELEPOINT (0,0,5)
```

### Creating Data Visualization

Syntax: [timeStamp] CREATE DATAVIS (position) (rotation) *graphName* (fileName)

Create a graph at position (0, 2, 0) with 0 degree rotation called Car, using file from Examples\scatterplot.json

```INS
CREATE DATAVIS (0,2,0) (0) Cars (Examples\scatterplot.json)
```

## Object Animation

Warning: *Only objects which created with names can be animated*

Warning: *Certain Predefined abstract objects can be manipulated using object animation, for a full list of abstract objects, see **Abstract Object***

Available Action: MOVE, RUNTO, WALKTO, PLAY_ANIMATION, SHOW, HIDE

### Basic Action (Applies for All Objects with Name)

Syntax: [timeStamp] *objectName* *action* (position) (rotationInDegree) (duration)

At 00:07, move MyCube to position (-4, 2, 4) with rotation 180 degree in 5 seconds:

```INS
[00:07] MyCube MOVE (-4,2,4) (180) (5)
```

### Walking and Running (Only Applies for Person with Name)

Syntax:

[timeStamp] *objectName* WALKTO (position)

[timeStamp] *objectName* RUNTO (position)

At 00:08 let Tommy run to position (0, 0, 0)

```INS
[00:08] Tommy RUNTO (0,0,0)
```

### Imported Object Animation (Only Applies for Imported Objects with Name)

Syntax: [timeStamp] *objectName* PLAY_ANIMATION *animationName(optional)*

At 00:01, play default animation for object called Squirrel_Imported:

```INS
[00:01] Squirrel_Imported PLAY_ANIMATION
```

At 00:02, play animation called take01_ani for object called Squirrel_Imported:

```INS
[00:01] Squirrel_Imported PLAY_ANIMATION take01_ani
```

## Show and Hide Object

Syntax: [timeStamp] *objectName* SHOW

Syntax: [timeStamp] *objectName* HIDE

At 02:08, hide object named Luka_Black and at 03:10, show(unhide) object named Luka_Black

```INS
[02:08] Luka_Black HIDE
[03:10] Luka_Black SHOW
```

## Sound and Subtitle

### Play Sound

Syntax: [timeStamp] PLAY_SOUND (fileName)*

At 00:00, play MyBGM.wav in my datafile root directory:

```INS
[00:00] PLAY_SOUND (MyBGM.wav)
```

### Display Subtitle

Syntax: [timeStamp] SUBTITLE (text) (duration)

At 01:05, show "Under my eyes" as subtitle for 10 seconds:

```INS
[01:05] SUBTITLE (Under my eyes) (10)
```

## Special Effects

### Change Time of Day

Syntax: [timeStamp] SET_TIMEOFDAY (timeOfDayInHour)

At 00:00, set time of day to 2 o'clock in the morning:

```INS
[00:00] SET_TIMEOFDAY (2)
```

### Fade In & Fade Out

Syntax:

[timeStamp] FADEIN (optionalColor) (duration)

[timeStamp] FADEIN (duration)

[timeStamp] FADEOUT (optionalColor) (duration)

[timeStamp] FADEOUT (duration)

At 00:00, fade in screen in 2 seconds using default white color, then at 00:07, fade out using black color (RGB value: 0, 0, 0) in 1 second:

```INS
[00:00] FADEIN (2)
[00:07] FADEOUT (0,0,0) (1)
```

## Abstract Objects

Abstract objects are pre-existed objects in all scenes without the needs to create them using command.

List of Abstract Objects:

- Player
