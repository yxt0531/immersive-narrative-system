# Immersive Narrative System Quickstart Guide

## Start Up

- Download the latest release under Release tab.
- Extract downloaded file using 7-Zip or other compatable archiver.
- (Oculus Rift Only) Start Oculus App.
- Start SteamVR.
- Open *Immersive Narrative System.exe* in the extracted folder to run.

## Basic UI Control

**Open** to open existing narrative datafile.
**Reset** to clear loaded narrative scene.
**Fast-forward** to increase playback speed, toggle from normal, 2x and 4x.
**Pause** to pause current narrative scene.
**Quit** to exit.

## VR Navigation

Push left or right stick on controller to display a teleporting line, release to teleport. (Only works when the loaded narrative datafile supports teleportation)

## Create Narrative Datafile

See *Datafile_Syntax.md* for details.
