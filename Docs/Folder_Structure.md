# Folder Structure

*This document is used for explaining folder structure of the project.*

## Modules

Modules should be stored as prefabs in the ***./Assets/Scripts*** folder, alone with its corrsponding scripts. For prefab with one script, it should be using the same name for both the prefab and the script. For prefab with multiple scripts, naming should be relative with format ***prefabName_scriptName***.

Example:

Prefab: ***GLTF***, Script: ***GLTF***

Prefab: ***UIController***, Script 1: ***UIController_ButtonController***, Script 2: ***UIController_SliderController***

## Hot-Load Modules

For module that require loading during runtime such as INS built-in person models, its prefab should be stored in ***./Assets/Resources*** folder.

## INS Built-In Objects

For all INS built-in objects, store in ***./Assets/Built-In Objects*** folder alone with their textures and materials. (*for its prefab, see Section **Hot-Load Modules***)

## Demo Scenes

Each demo scene should contain only one module for demonstration. The demo scenes should be stored in ***./Assets/Scene/DemoScenes*** folder. Naming should be relavant to its corrsponding module. All the scripts used by the demo scene should be stored in the same folder ***Scene/DemoScenes*** as well.

Example:

Demo Scene: ***GLTFImportModuleDemo***, Script: ***GLTFImportDemoUIController***

## Documentations

All documentations should be stored in the ***./Docs*** folder, for text document, use Markdown format, for document with pictures, use PDF format.

## Testing Files

All testing files such as demo datafile and 3D models should be stored in the ***./Testing*** folder.
